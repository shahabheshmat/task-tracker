import Button from "./Button";
import { useLocation } from "react-router-dom";
const Header = ({onAdd, taskButton}) => {
    const location = useLocation()
    return (
        <header className='header'>
            <h1 >HD task tracker </h1>
            {location.pathname ==="/" && <Button text={taskButton ? "Close" : "Add"} onClick={onAdd}/>}
        </header>
    )
}


export default Header
